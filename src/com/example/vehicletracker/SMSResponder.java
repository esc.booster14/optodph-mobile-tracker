package com.example.vehicletracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SMSResponder extends BroadcastReceiver implements LocationListener{
	
	//location related variables
	LocationManager locman;
	Criteria criteria;
	String provider;
	Location location;
	double lat,lng;
	
	SessionManager session;

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		
		session = new SessionManager(arg0);
		
		Bundle b=arg1.getExtras();
		Object[] pdu = (Object[]) b.get("pdus");
		for(int i=0;i<pdu.length;i++){
			SmsMessage msg = SmsMessage.createFromPdu((byte[])pdu[i]);			
			String sender = msg.getDisplayOriginatingAddress();
//			String message=msg.getDisplayMessageBody();
			String reply="";
			
			locman=(LocationManager) arg0.getSystemService(Context.LOCATION_SERVICE);
			criteria=new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_MEDIUM);
			criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
			provider=locman.getBestProvider(criteria, false);
			locman.requestLocationUpdates(provider, 0, 0, this);
			location=locman.getLastKnownLocation(provider);
			
			String vehicle = session.getUserDetails().get("vehicle");
			String phone = session.getUserDetails().get("phone");
			
			if(location!=null && !vehicle.equals("")){
				lat=location.getLatitude();
				lng=location.getLongitude();
				reply="https://www.latlong.net/c/?lat="+ lat +"&long="+lng;
				SmsManager manager = SmsManager.getDefault();
				manager.sendTextMessage(phone,"",reply, null, null);
				
//				new RequestTask().execute("http://192.168.1.6/mob/vehicle/location-setup/"+vehicle+"?lat="+ lat +"&long="+lng);
				new RequestTask().execute("http://192.168.254.103/mob/vehicle/location-setup/"+vehicle+"?lat="+ lat +"&long="+lng);
				
//				this.toastL(arg0, "A message contained your location will be sent to your operator.");
			}			
		}		
	}
	
	public void toastS(Context arg0, String str){
		Toast.makeText(arg0, str, Toast.LENGTH_SHORT).show();
	}

	public void toastL(Context arg0, String str){
		Toast.makeText(arg0, str, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
}
