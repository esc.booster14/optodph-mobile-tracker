package com.example.vehicletracker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class LoginRequest {
	
	String name, vehicle, phone;
	String username, password;
	
	public LoginRequest(String username, String password) {		
		this.username = username;
		this.password = password;
		
		this.name = this.sendRequest("gname");
		this.vehicle = this.sendRequest("gvehicletoken");
		this.phone = this.sendRequest("gphone");
	}

	public String sendRequest(String url){
		
		HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet("http://192.168.1.6/mob/login/"+ url +"?username="+ this.username +"&password="+ this.password));
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                responseString = out.toString();
                out.close();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
        } catch (IOException e) {
            //TODO Handle problems..
        }
        
        return responseString;
	}
}
