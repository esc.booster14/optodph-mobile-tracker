package com.example.vehicletracker;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {
	
	TextView tv1, tv2, tvUsername, tvPassword;
	EditText etUsername, etPassword;
	Button btn;
	
	SessionManager session;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        session = new SessionManager(getApplicationContext());
        
        this.tv1 =  (TextView) this.findViewById(R.id.TextView01);
        this.tv2 =  (TextView) this.findViewById(R.id.textView1);
        this.tvUsername =  (TextView) this.findViewById(R.id.textView2);
        this.tvPassword =  (TextView) this.findViewById(R.id.TextView02);
        
        this.etUsername = (EditText) this.findViewById(R.id.editText1);
        this.etPassword = (EditText) this.findViewById(R.id.editText2);
        
        this.btn = (Button) this.findViewById(R.id.button1);
        this.btn.setOnClickListener(this);
    }

	@Override
	public void onClick(View arg0) {
		String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        
        LoginRequest request = new LoginRequest(username, password);
        
        if(username.trim().length() > 0 && password.trim().length() > 0){
        	if(!request.name.equals("")){
        		session.createLoginSession(request.name, request.vehicle, request.phone);
        		
        		Intent intent = new Intent(this, MainActivity.class);
        		startActivity(intent);
        		
        		this.toastS("You successfully logged-in!!");
        	}else this.toastL("These credentials does not match our records.");
        }else this.toastL("Please fill-in the username and password.");
	}

	public void toastS(String str){
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

	public void toastL(String str){
		Toast.makeText(this, str, Toast.LENGTH_LONG).show();
	}
}
