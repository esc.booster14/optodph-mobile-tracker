package com.example.vehicletracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	
	SessionManager session;
	
	Button btnLogout;
	TextView tv1;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        session = new SessionManager(getApplicationContext());
        
        this.tv1 = (TextView) this.findViewById(R.id.textView1);
        this.btnLogout = (Button) this.findViewById(R.id.button1);
        
        session.checkLogin();
        
        String name = session.getUserDetails().get("name");
        this.tv1.setText("Hi "+name+"!! You are logged-in to vehicle tracker. You'll be tracked down whenever your renting the vehicle.");
        
        this.btnLogout.setOnClickListener(this);
    }

	@Override
	public void onClick(View arg0) {
		new AlertDialog.Builder(this)
		.setTitle("Logout")
		.setMessage("Are you sure you want to logout?")
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int whichButton) {
		    	MainActivity.this.toastS("You are logging-out...");
		    	MainActivity.this.session.logoutUser();
		    }})
		 .setNegativeButton(android.R.string.no, null).show();
	}
	
	public void toastS(String str){
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

	public void toastL(String str){
		Toast.makeText(this, str, Toast.LENGTH_LONG).show();
	}
}
